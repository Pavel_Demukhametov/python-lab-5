import random
import string

import segno
import io


for length in range(1, 11):
    qrcode = segno.make('a' * length)
    buffer = io.StringIO()
    qrcode.save(buffer, kind="txt", border=0)
    result = buffer.getvalue()
max_length = 9


dataset = []

for i in range(20):
    length = 9
    text = ''.join(random.choices(string.ascii_uppercase, k=length))
    qrcode = segno.make(text)
    matrix = qrcode.matrix
    flat_array = [pixel for row in matrix for pixel in row]
    dataset.append([flat_array, 1])
for i in range(100):
    # Генерируем случайный массив того же размера
    arr = [random.randint(0, 1) for i in range(len(dataset[0][0]))]
    # Добавляем в выборку с меткой 0
    dataset.append([arr, 0])

n_sensor = 225
weight = [0 for i in range(n_sensor)]

def perceptron(Sensor):
    b = 1000
    s = 0
    for i in range(n_sensor):
        s += int(Sensor[i]) * weight[i]
    if s >= b:
        return True
    else:
        return False

def increase(number):
    for i in range(n_sensor):
        if int(number[i]) == 1:
            weight[i] += 1

def decrease(number):
    for i in range(n_sensor):
        if int(number[i]) == 1:
            weight[i] -= 1

n = 20
print()
for i in range(n):
    rows = list(range(len(dataset)))
    random.shuffle(rows)
    new_arr = [dataset[i] for i in rows]

    for j in new_arr:
        data = j[0]
        r = j[1]
        res = perceptron(data)
        if r:
            if r != res:
                increase(data)
        else:
            if r != res:
                decrease(data)
for i in dataset:
    print(i[1], perceptron(i[0]))
print(weight)